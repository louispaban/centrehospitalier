/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projetjava;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author louispaban
 */

public class RechercheInfo {

    
private String numeroChambre, codeService, surveillant, nb_lits, numeroEmploye, specialite, nom, prenom, adresse, numeroTelephone, numeroMalade, lit, rotation, salaire, mutuelle, code, numeroDocteur, batiment, directeur;
ArrayList <String> listeRequetes;
String requete;
Connexion conne ;


    public RechercheInfo(){
        try{
            
            Connexion conn = new Connexion("hopital","root","");
            listeRequetes = conn.remplirChampsRequete(requete);
            System.out.println(listeRequetes);
            
        }catch (SQLException | ClassNotFoundException e) {}

    }
    

    
    
public ArrayList <String> rechercheChambre(String codeService, String numeroChambre, String surveillant, String nb_lits) throws SQLException, ClassNotFoundException{
        
    ArrayList <String> comparer= new ArrayList<>();
    //comparer.add("");
    /*ArrayList<String> al3= new ArrayList<String>();
    comparer.add(codeService);
    comparer.add(numeroChambre);
    comparer.add(surveillant);
    comparer.add(nb_lits); */
    Connexion conn =new Connexion("hopital", "root", "");
    requete = "SELECT * FROM chambre WHERE true";
    if(!codeService.equals("")){ requete+=" AND code_service = '"+codeService+"'";}
    if(!numeroChambre.equals("")){ requete+=" AND no_chambre= '"+numeroChambre+"'";}
    if(!surveillant.equals("")){ requete+=" AND surveillant = '"+surveillant+"'";}
    if(!nb_lits.equals("")){ requete+=" AND nb_lits = '"+nb_lits+"'";}
   
    System.out.println(requete);
   listeRequetes = conn.remplirChampsRequete(requete);
   
   System.out.println(listeRequetes);
  
    // System.out.println(comparer);
    /*for (String temp : listeRequetes)
    al3.add(comparer.contains(temp)?"Oui":"Non");
    System.out.println(al3); */
    
  // System.out.println(comparer);
 /*for (String temp : listeRequetes)
     al3.add(comparer.contains(temp)?"Oui":"Non");
 System.out.println(al3); */
    
    return listeRequetes;

}

public ArrayList <String> rechercheDocteur(String numeroEmploye, String specialite) throws SQLException, ClassNotFoundException{
     Connexion conn =new Connexion("hopital", "root", "");   
    requete = "SELECT * FROM docteur WHERE true";
    if(!numeroEmploye.equals("")){ requete+=" AND numero = '"+numeroEmploye+"'";}
    if(!specialite.equals("")){ requete+=" AND specialite = '"+specialite+"'";}
   
    System.out.println(requete);
    listeRequetes = conn.remplirChampsRequete(requete);
    System.out.println(listeRequetes);   
    return listeRequetes;
    

}    
    
    
    
    
public ArrayList <String> rechercheEmploye(String numeroEmploye, String nom, String prenom, String adresse, String numeroTelephone) throws SQLException, ClassNotFoundException{
    Connexion conn =new Connexion("hopital", "root", "");    
    requete = "SELECT * FROM employe WHERE true";
    if(!numeroEmploye.equals("")){ requete+=" AND numero = '"+numeroEmploye+"'";}
    if(!nom.equals("")){ requete+=" AND nom = '"+nom+"'";}
    if(!prenom.equals("")){ requete+=" AND prenom = '"+prenom+"'";}
    if(!adresse.equals("")){ requete+=" AND adresse = '"+adresse+"'";}
    if(!numeroTelephone.equals("")){ requete+=" AND tel = '"+numeroTelephone+"'";}
    System.out.println(requete);
    listeRequetes = conn.remplirChampsRequete(requete);
    System.out.println(listeRequetes);    
    return listeRequetes;
    

}
    

   
    
public ArrayList <String> rechercheHospitalisation(String numeroMalade, String codeService, String numeroChambre, String lit) throws SQLException, ClassNotFoundException{
    Connexion conn =new Connexion("hopital", "root", "");    
    requete = "SELECT * FROM hospitalisation WHERE true";
    if(!numeroMalade.equals("")){ requete+=" AND no_malade = '"+numeroMalade+"'";}
    if(!codeService.equals("")){ requete+=" AND code_service = '"+codeService+"'";}
    if(!numeroChambre.equals("")){ requete+=" AND no_chambre = '"+numeroChambre+"'";}
    if(!lit.equals("")){ requete+=" AND lit = '"+lit+"'";}
    
    System.out.println(requete);
    listeRequetes = conn.remplirChampsRequete(requete);
    System.out.println(listeRequetes);
        
    return listeRequetes;
    

}
    
    
public ArrayList <String> rechercheInfirmier(String numeroEmploye, String codeService, String rotation, String salaire) throws SQLException, ClassNotFoundException{
     Connexion conn =new Connexion("hopital", "root", "");   
    requete = "SELECT * FROM infirmier WHERE true";
    if(!numeroEmploye.equals("")){ requete+=" AND numero = '"+numeroEmploye+"'";}
    if(!codeService.equals("")){ requete+=" AND code_service = '"+codeService+"'";}
    if(!rotation.equals("")){ requete+=" AND rotation = '"+rotation+"'";}
    if(!salaire.equals("")){ requete+=" AND salaire = '"+salaire+"'";}
    
    System.out.println(requete);
    listeRequetes = conn.remplirChampsRequete(requete);
    System.out.println(listeRequetes);
        
    return listeRequetes;
    

}
    
    
public ArrayList <String> rechercheMalade(String numeroEmploye, String nom, String prenom, String adresse, String numeroTelephone, String mutuelle) throws SQLException, ClassNotFoundException{
    Connexion conn =new Connexion("hopital", "root", "");    
    requete = "SELECT * FROM malade WHERE true";
    if(!numeroEmploye.equals("")){ requete+=" AND numero = '"+numeroEmploye+"'";}
    if(!nom.equals("")){ requete+=" AND nom = '"+nom+"'";}
    if(!prenom.equals("")){ requete+=" AND prenom = '"+prenom+"'";}
    if(!adresse.equals("")){ requete+=" AND adresse = '"+adresse+"'";}
    if(!numeroTelephone.equals("")){ requete+=" AND tel = '"+numeroTelephone+"'";}
    if(!mutuelle.equals("")){ requete+=" AND mutuelle = '"+mutuelle+"'";}
    System.out.println(requete);
    listeRequetes = conn.remplirChampsRequete(requete);
    System.out.println(listeRequetes);    
    return listeRequetes;
    

}
    
public ArrayList <String> rechercheService(String code, String nom, String batiment, String directeur) throws SQLException, ClassNotFoundException{
     Connexion conn =new Connexion("hopital", "root", "");  
    requete = "SELECT * FROM service WHERE true";
    if(!code.equals("")){ requete+=" AND code = '"+code+"'";}
    if(!nom.equals("")){ requete+=" AND nom = '"+nom+"'";}
    if(!batiment.equals("")){ requete+=" AND batiment = '"+batiment+"'";}
    if(!directeur.equals("")){ requete+=" AND directeur = '"+directeur+"'";}
    
    System.out.println(requete);
    listeRequetes = conn.remplirChampsRequete(requete);
    System.out.println(listeRequetes);    
    return listeRequetes;
    

}

public ArrayList <String> rechercheSoigne(String numeroDocteur, String numeroMalade) throws SQLException, ClassNotFoundException{
    Connexion conn =new Connexion("hopital", "root", "");    
    requete = "SELECT * FROM soigne WHERE true";
    if(!numeroDocteur.equals("")){ requete+=" AND no_docteur = '"+numeroDocteur+"'";}
    if(!numeroMalade.equals("")){ requete+=" AND no_malade = '"+numeroMalade+"'";}
    
    
    System.out.println(requete);
    listeRequetes = conn.remplirChampsRequete(requete);
    System.out.println(listeRequetes);    
    return listeRequetes;
    

}


    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projetjava;

/**
 *
 * @author louispaban
 */

public class Hospitalisation {
    
    private int numeroMalade;
    private int codeService;
    private int numeroChambre;
    private int lit;
    
public Hospitalisation() {
        
    numeroMalade =0;
    codeService = 0;
    numeroChambre= 0;
    lit = 0;
}
    
public Hospitalisation(int numeroMalade, int codeService, int numeroChambre, int lit) {
        
    this.numeroMalade = numeroMalade;
    this.codeService = codeService;
    this.numeroChambre = numeroChambre;
    this.lit = lit;
}

public int getNumeroMalade() {
        return numeroMalade;
    }

    
public void setNumeroMalade(int numeroMalade) {
        this.numeroMalade = numeroMalade;
    }

public int getCodeService() {
        return codeService;
    }

   
public void setCodeService(int codeService) {
        this.codeService = codeService;
    }


    public int getNumeroChambre() {
        return numeroChambre;
    }


    public void setNumeroChambre(int numeroChambre) {
        this.numeroChambre = numeroChambre;
    }

    public int getLit() {
        return lit;
    }

    public void setLit(int lit) {
        this.lit = lit;
    }
    
    
}


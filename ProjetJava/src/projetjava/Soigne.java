/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projetjava;

/**
 *
 * @author louispaban
 */
public class Soigne {
    
    private int numeroDocteur;
    private int numeroMalade;
    
public Soigne() {
    
    numeroDocteur = 0;
    numeroMalade = 0;   
    }
    
public Soigne(int numeroDocteur, int numeroMalade) {
        this.numeroDocteur = numeroDocteur;
        this.numeroMalade = numeroMalade;
    }



public int getNumeroDocteur() {
        return numeroDocteur;
    }

public void setNumeroDocteur(int numeroDocteur) {
        this.numeroDocteur = numeroDocteur;
    }


public int getNumeroMalade() {
        return numeroMalade;
    }


public void setNumeroMalade(int numeroMalade) {
        this.numeroMalade = numeroMalade;
    }
}


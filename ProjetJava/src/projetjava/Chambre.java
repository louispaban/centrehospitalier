/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projetjava;

/**
 *
 * @author louispaban
 */

public class Chambre {

/* attributs de la classe Chambre
 *
 */
    private int numeroChambre;
    private int codeService;
    private int surveillant; 
    private int nb_lits;
    
/*
 * constructeur par défaut inatialisant les attributs
 */

public Chambre() {
        
    numeroChambre = 0;
    codeService = 0;
    surveillant = 0;
    nb_lits = 0;
    }

/*
 * constructeur surchargé
 */

public Chambre(int numero, int codeService, int surveillant, int nb_lits) {
        
    this.numeroChambre = numero;
    this.codeService = codeService;
    this.surveillant = surveillant;
    this.nb_lits = nb_lits;
    }
    


/*
 * Accesseurs pour l'attribut numeroChambre
 */

public int getNumeroChambre() {
        return numeroChambre;
}

 public void setNumeroChambre(int numeroChambre) {
        this.numeroChambre = numeroChambre;
    }


/*
 * Accesseurs pour l'attribut codeService
 */
 
 
 public int getCodService() {
        return codeService;
    }

   
    public void setCodeService(int codeService) {
        this.codeService = codeService;
    }




/*
 * Accesseurs pour l'attribut surveillant
 */


public int getSurveillant() {
        return surveillant;
    }

    
public void setSurveillant(int surveillant) {
        this.surveillant = surveillant;
    }



/*
 * Accesseurs pour l'attribut nb_lits
 */

public int getNb_lits() {
        return nb_lits;
    }

    
public void setNb_lits(int nb_lits) {
        this.nb_lits = nb_lits;
    }




}


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projetjava;

import com.mysql.jdbc.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.*;
import java.util.ArrayList;


/**
 *
 * @author louispaban
 */
public class MiseàJour {
    
Connexion con=null;
Statement stmt;

    
public MiseàJour() throws SQLException, ClassNotFoundException {
        this.con = new Connexion("hopital","root","");
    }

// méthode d'ajout pour la classe Chambre et mettre les attributs de Chambre en public
    
public Statement ajoutChambre ( String codeService,String numeroChambre, String surveillant, String nb_lits) throws SQLException {

//stmt à mettre en public dans la classe connexion de segado
stmt=con.conn.createStatement();
stmt.executeUpdate(" insert into chambre(code_service,no_chambre, surveillant, nb_lits) values ('"+codeService+"','"+numeroChambre+"',  '"+surveillant+"', '"+nb_lits+"')");
return stmt;    
    
}    
    
// méthode de suppression pour la classe Chambre et mettre les attributs de Chambre en public
    
public Statement suppressionChambre (String codeService, String numeroChambre, String surveillant, String nb_lits) throws SQLException {

//stmt à mettre en public dans la classe connexion de segado
stmt=con.conn.createStatement();
stmt.executeUpdate("delete from chambre where code_service='"+codeService+"' AND no_chambre='"+numeroChambre+"' AND surveillant='"+surveillant+"' AND nb_lits='"+nb_lits+"'");
System.out.println("supprimé");
return stmt;    
}

// méthode d'ajout pour la classe Docteur et mettre les attributs de Docteur en public
    
public Statement ajoutDocteur (String numeroEmploye, String specialite) throws SQLException {

//stmt à mettre en public dans la classe connexion de segado
stmt=con.conn.createStatement();
stmt.executeUpdate(" insert into docteur(numero,specialite) values ('"+numeroEmploye+"', '"+specialite+"')");
return stmt;    


}    
    
// méthode de suppression pour la classe Docteur et mettre les attributs de Docteur en public
    
public Statement suppressionDocteur (String numeroEmploye, String specialite) throws SQLException {

//stmt à mettre en public dans la classe connexion de segado
stmt=con.conn.createStatement();
stmt.executeUpdate("delete from docteur where numero='"+numeroEmploye+"' AND specialite='"+specialite+"'");   return stmt;
}


// méthode d'ajout pour la classe Employe et mettre les attributs de Employe en public
    
public Statement ajoutEmploye (String numeroEmploye, String nom, String prenom, String numeroTelephone, String adresse) throws SQLException {

//stmt à mettre en public dans la classe connexion de segado
stmt=con.conn.createStatement();
stmt.executeUpdate(" insert into employe(numero, nom, prenom,adresse,tel) values ('"+numeroEmploye+"', '"+nom+"', '"+prenom+"', '"+numeroTelephone+"','"+adresse+"')");
return stmt;    
    
}    
    
// méthode de suppression pour la classe Employe et mettre les attributs de Employe en public
    
public Statement suppressionEmploye (String numeroEmploye, String nom, String prenom, String numeroTelephone, String adresse) throws SQLException {

//stmt à mettre en public dans la classe connexion de segado
stmt=con.conn.createStatement();
stmt.executeUpdate("delete from employe where numero='"+numeroEmploye+"' AND nom='"+nom+"' AND prenom='"+prenom+"' AND tel='"+numeroTelephone+"' AND adresse='"+adresse+"'");
return stmt;

} 


// méthode d'ajout pour la classe Hospitalisation et mettre les attributs de Hospitalisation en public
    
public Statement ajoutHospitalisation (String numeroMalade, String codeService, String numeroChambre, String lit) throws SQLException {

//stmt à mettre en public dans la classe connexion de segado
stmt=con.conn.createStatement();
stmt.executeUpdate(" insert into hospitalisation (no_malade, code_service, no_chambre, lit) values ('"+numeroMalade+"', '"+codeService+"', '"+numeroChambre+"', '"+lit+"')");
return stmt;    
    
}    
    
// méthode de suppression pour la classe Hospitalisation et mettre les attributs de Hospitalisation en public
    
public Statement suppressionHospitalisaton (String numeroMalade, String codeService, String numeroChambre, String lit) throws SQLException {

//stmt à mettre en public dans la classe connexion de segado
stmt=con.conn.createStatement();
stmt.executeUpdate("delete from hospitalisation where no_malade='"+numeroMalade+"' AND code_service='"+codeService+"' AND no_chambre='"+numeroChambre+"' AND lit='"+lit+"'");
return stmt; 

}

// méthode d'ajout pour la classe Infirmier et mettre les attributs de Infirmier en public
    
public Statement ajoutInfirmier (String numeroEmploye, String codeService, String rotation, String salaire) throws SQLException {

//stmt à mettre en public dans la classe connexion de segado
stmt=con.conn.createStatement();
stmt.executeUpdate(" insert into infirmier(numero, code_service,rotation, salaire) values ('"+numeroEmploye+"', '"+codeService+"', '"+rotation+"', '"+salaire+"')");
return stmt;    
    
}    
    
// méthode de suppression pour la classe Infirmier et mettre les attributs de Infirmier en public
    
public Statement suppressionInfirmier (String numeroEmploye, String codeService, String rotation, String salaire) throws SQLException {

//stmt à mettre en public dans la classe connexion de segado
stmt=con.conn.createStatement();
stmt.executeUpdate("delete from infirmier where numero='"+numeroEmploye+"' AND code_service='"+codeService+"' AND rotation='"+rotation+"' AND salaire='"+salaire+"'");
return stmt;
} 


// méthode d'ajout pour la classe Malade et mettre les attributs de Malade en public
    
public Statement ajoutMalade (String numeroEmploye, String nom, String prenom, String numeroTelephone, String adresse, String mutuelle) throws SQLException {

//stmt à mettre en public dans la classe connexion de segado
stmt=con.conn.createStatement();
stmt.executeUpdate(" insert into malade(numero, nom, prenom,adresse,tel,mutuelle) values ('"+numeroEmploye+"', '"+nom+"', '"+prenom+"', '"+numeroTelephone+"','"+adresse+"', '"+mutuelle+"')");
return stmt;    
    
}    
    
// méthode de suppression pour la classe Malade et mettre les attributs de Malade en public
    
public Statement suppressionMalade (String numeroEmploye, String nom, String prenom, String numeroTelephone, String adresse, String mutuelle) throws SQLException {

//stmt à mettre en public dans la classe connexion de segado
stmt=con.conn.createStatement();
stmt.executeUpdate("delete from malade where numero='"+numeroEmploye+"' AND nom='"+nom+"' AND prenom='"+prenom+"' AND tel='"+numeroTelephone+"' AND adresse='"+adresse+"' AND mutuelle='"+mutuelle+"'" );
return stmt;
} 

// méthode d'ajout pour la classe Service et mettre les attributs de Service en public
    
public Statement ajoutService (String code, String nom, String batiment, String directeur) throws SQLException {

//stmt à mettre en public dans la classe connexion de segado
stmt=con.conn.createStatement();
stmt.executeUpdate(" insert into service(code, nom, batiment, directeur) values ('"+code+"', '"+nom+"', '"+batiment+"', '"+directeur+"')");
return stmt;    
}
       
// méthode de suppression pour la classe Service et mettre les attributs de Service en public
    
public Statement suppressionService (String code, String nom, String batiment, String directeur) throws SQLException {

//stmt à mettre en public dans la classe connexion de segado
stmt=con.conn.createStatement();
stmt.executeUpdate("delete from service where code='"+code+"' AND nom='"+nom+"' AND batiment='"+batiment+"' AND directeur='"+directeur+"'" );
return stmt;

}



// méthode d'ajout pour la classe Soigne et mettre les attributs de Soigne en public
    
public Statement ajoutSoigne (String numeroDocteur, String numeroMalade) throws SQLException {

//stmt à mettre en public dans la classe connexion de segado
stmt=con.conn.createStatement();
stmt.executeUpdate(" insert into soigne(no_docteur, no_malade) values ('"+numeroDocteur+"', '"+numeroMalade+"')");
return stmt;    
 
}    
    
// méthode de suppression pour la classe Soigne et mettre les attributs de Soigne en public
    
public Statement suppressionSoigne (String numeroDocteur, String numeroMalade) throws SQLException {

//stmt à mettre en public dans la classe connexion de segado
stmt=con.conn.createStatement();
stmt.executeUpdate("delete from soigne where no_docteur = '"+numeroDocteur+"' AND no_malade ='"+numeroMalade+"'");
return stmt;

}    


public Statement modifierChambre ( String codeService,String numeroChambre, String surveillant, String nb_lits) throws SQLException {

//stmt à mettre en public dans la classe connexion de segado
stmt=con.conn.createStatement();
stmt.executeUpdate(" UPDATE chambre SET code_service='"+codeService+"', no_chambre='"+numeroChambre+"', surveillant='"+surveillant+"'WHERE nb_lits='"+nb_lits+"'");
return stmt;    
    
} 


public Statement modifierDocteur (String numeroEmploye, String spec) throws SQLException {

//stmt à mettre en public dans la classe connexion de segado
stmt=con.conn.createStatement();
stmt.executeUpdate(" UPDATE docteur SET numero='"+numeroEmploye+"'WHERE specialite='"+spec+"'");
return stmt;    
    
}


public Statement modifierEmploye (String numeroEmploye, String nom, String prenom, String numeroTelephone, String adresse) throws SQLException {

//stmt à mettre en public dans la classe connexion de segado
stmt=con.conn.createStatement();
stmt.executeUpdate(" UPDATE employe SET numero='"+numeroEmploye+"', nom='"+nom+"', prenom='"+prenom+"', tel='"+numeroTelephone+"'WHERE adresse='"+adresse+"'");
return stmt;    
    
} 


public Statement modifierHospitalisation (String numeroMalade, String codeService, String numeroChambre, String lit) throws SQLException {

//stmt à mettre en public dans la classe connexion de segado
stmt=con.conn.createStatement();
stmt.executeUpdate(" UPDATE employe SET no_malade='"+numeroMalade+"', code_service='"+codeService+"', no_chambre='"+numeroChambre+"'WHERE lit='"+lit+"'");
return stmt;    
    
} 

public Statement modifierInfirmier (String numeroEmploye, String codeService, String rotation, String salaire) throws SQLException {

//stmt à mettre en public dans la classe connexion de segado
stmt=con.conn.createStatement();
stmt.executeUpdate(" UPDATE employe SET numero='"+numeroEmploye+"', code_service='"+codeService+"', rotation='"+rotation+"'WHERE salaire='"+salaire+"'");
return stmt;    
    
} 


public Statement modifierMalade (String numeroEmploye, String nom, String prenom, String numeroTelephone, String adresse, String mutuelle) throws SQLException {

//stmt à mettre en public dans la classe connexion de segado
stmt=con.conn.createStatement();
stmt.executeUpdate(" UPDATE employe SET numero='"+numeroEmploye+"', nom='"+nom+"', prenom='"+prenom+"', tel='"+numeroTelephone+"', adresse='"+adresse+"'WHERE mutuelle='"+mutuelle+"'");
return stmt;    
    
} 



public Statement modifierService (String code, String nom, String batiment, String directeur) throws SQLException {

//stmt à mettre en public dans la classe connexion de segado
stmt=con.conn.createStatement();
stmt.executeUpdate(" UPDATE employe SET code='"+code+"', nom='"+nom+"', batiment='"+batiment+"'WHERE directeur='"+directeur+"'");
return stmt; 



}


public Statement modifierSoigne (String numeroDocteur, String numeroMalade) throws SQLException {

//stmt à mettre en public dans la classe connexion de segado
stmt=con.conn.createStatement();
stmt.executeUpdate(" UPDATE employe SET no_docteur='"+numeroDocteur+"'WHERE no_malade='"+numeroMalade+"'");
return stmt; 



}





}
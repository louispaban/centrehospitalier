/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projetjava;

/**
 *
 * @author louispaban
 */
public class Employe {
    
/*
 * Attributs de la classe mère Employe
 */
    protected int numeroEmploye;
    protected String nom;
    protected String prenom;
    protected int numeroTelephone;
    protected String adresse;
    
/*
 * Constructeur par défaut
 */   
    
public Employe() {
     
    numeroEmploye = 0;
    nom = null;
    prenom = null;
    numeroTelephone = 0;
    adresse = null;
    }
    
/*
 * Constructeur surchargé
 */
   
public Employe(int numeroEmploye, String nom, String prenom, int numeroTelephone, String adresse) {
       
    this.numeroEmploye = numeroEmploye;
    this.nom = nom;
    this.prenom = prenom;
    this.numeroTelephone = numeroTelephone;
    this.adresse = adresse;
    }
    
/*
 * Accesseurs sur l'attribut numeroEmploye
 */

 public int getNumeroEmploye() {
        return numeroEmploye;
    }

 
    public void setNumerEmploye(int numeroEmploye) {
        this.numeroEmploye = numeroEmploye;
    }

/*
 * Accesseurs sur l'attribut nom
 */
    public String getNom() {
        return nom;
    }

   
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * Accesseurs sur prenom
     * @return 
     */
    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

     /**
     * Getter sur numeroTelephone qui retourne cette attribut
     * @return 
     */
    public int getNumeroTelephone() {
        return numeroTelephone;
    }

    /**
     * Setter
     * @param numeroTelephone
     */
    public void setNumeroTelephone(int numeroTelephone) {
        this.numeroTelephone = numeroTelephone;
    }

    /**
     * getter adresse
     * 
     */
    public String getAdresse() {
        return adresse;
    }

    /**
     * Setter adresse
     */
    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }
}


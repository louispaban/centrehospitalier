/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projetjava;

import projetjava.Employe;

/**
 *
 * @author louispaban
 */
public class Malade extends Employe {
    
    private String mutuelle;
    
public Malade() {
        
    super();
    mutuelle = null;
    }
    
public Malade(int numeroEmploye, String nom, String prenom, int numeroTelephone, String adresse, String mutuelle) {
       
    super(numeroEmploye, nom, prenom, numeroTelephone, adresse);
    this.mutuelle = mutuelle;
        
    }

public int getNumeroEmploye() {
        return numeroEmploye;
    }

  
public void setNumeroEmploye(int numeroEmploye) {
        this.numeroEmploye = numeroEmploye;
    }

   

    @Override
    public String getNom() {
        return nom;
    }


    
    public void setNom(String nom) {
        this.nom = nom;
    }

    
    public String getPrenom() {
        return prenom;
    }

    
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    
    public int getNumeroTelephone() {
        return numeroTelephone;
    }

 
    
    public void setNumeroTelephone(int numeroTelephone) {
        this.numeroTelephone = numeroTelephone;
    }

    public String getAdresse() {
        return adresse;
    }


    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

 
    public String getMutuelle() {
        return mutuelle;
    }

  
    public void setMutuelle(String mutuelle) {
        this.mutuelle = mutuelle;
    }
}




  


